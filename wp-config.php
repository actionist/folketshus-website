<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rinkeby-folkets-hus');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>l)90j_vCA@m!rqu:`5?KA)uYfGsLqgO04lkY$TsV5j^cWO)z::;(&Ybat UNS>u');
define('SECURE_AUTH_KEY',  '6>0hGh;s.I48,0uzC[N!=?(H6^#fn4-!i}4IHz?%yB&FPyx*1LLj`?gvCUBwjSfK');
define('LOGGED_IN_KEY',    ':w,|PLryF[7p6QvoA.X^eGkn58~z>#JsM1Xg<iyUHjAI=_6_Xva1$.oF&03I>_D@');
define('NONCE_KEY',        '*sn}@zv?vU<;xf`6EecAHXh!lJHjR$5}dXd,$z;%&4{,r=-[R/ya)#Tk&~l?vJ)a');
define('AUTH_SALT',        '18A+H.:=ZbUQiQ{dghVCb#{pHq:|D|;7xRF!9LN?;A{uHNG^PX -&z:,}Cp}S(]i');
define('SECURE_AUTH_SALT', '?<Z|hke3b[1(2`e?BOCO#=BqAuZd13`$Y_=Z_>V?w<$A4K[:xDF>6QJ0aojy[I5Z');
define('LOGGED_IN_SALT',   'Ll<3z|wqLi*~At~5`E6ph*$Br^!AYXePC]U%jQHCgDbe0)s{!q;c31C5+:cE=&Qg');
define('NONCE_SALT',       'ntK(cv0T>>@Gq8fHLE?]&rxi.KIUTWTN<ID606)gyb&HEST7x>w}:R{~}SH %K[E');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
