(function($, root, undefined) {

  $(function() {

    // Venues Ajax
    //Alla bilder
    var venueList = $('.puff__container--venue').children();
    //den som visas stort nu
    var currentShowing = $('.venue__wrapper').attr('id');
    //småbilder som syns, är aktiva i listan
    var activeItems = $('.venue--active').children();

    $('.puff__venue').on('click', function(e) {
      e.preventDefault();
      var target = this;
      var targetId = $(target).attr('id');

      var inactiveId = $('.venue--inactive').attr('id');

      $('#' + targetId).addClass('venue--inactive');
      $('#' + inactiveId).removeClass('venue--inactive')

      $('#' + targetId).removeClass('venue--active');
      $('#' + inactiveId).addClass('venue--active')


      $.ajax({
        url: ajaxobject.ajax_url,
        type: 'post',
        data: {
          action: 'ajax_call',
          id: targetId
        },
        beforeSend: function(){
          $('#single-venue').find('.venue__wrapper').remove();
        },
        success: function(response){
          $("#single-venue").html(response);


        }
      })
    });

  });

})(jQuery, this);
