<?php
/* Template Name: På gång */

get_header();
?>
<main role="main" class="wrapper__main">
	<section class="section span_12_of_12 category__wrapper">
		<?php
		if (have_posts()): while (have_posts()) : the_post(); ?>
			<h1 class="page__title"><?php the_title(); ?></h1>
	<?php
			endwhile;
		endif;
		?>

	  <?php
		$postLoop = new WP_Query(array(
			'post_type' => 'post',
			'posts_per_page' => -1,
			'meta_key' => 'datum_for_handelse',
			'orderby' => 'meta_value',
			'order' => 'DESC'
		));
		if ($postLoop->have_posts()): while ($postLoop->have_posts()) : $postLoop->the_post(); ?>
	    <?php if ( has_post_thumbnail()) :
	      $thumb_id = get_post_thumbnail_id($post->ID);
	      $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
	      $thumb_url = $thumb_url_array[0];

	    endif;
			$count++;
			$even_odd_class = ( ($count % 2) == 0 ) ? "even" : "odd";
	    ?>
	    <section class="section span_12_of_12 category__post <?php echo $even_odd_class; ?>">
	      <?php if ( has_post_thumbnail()) : ?>
	        <div class="category__img span_4_of_12" style="background-image:url(<?php echo $thumb_url; ?>);">
	        </div>
	      <?php
				$section_spans = 'span_8_of_12';
				else :
				$section_spans = 'span_12_of_12';
				endif;
	      ?>
	      <section class="category__content <?php echo $section_spans; ?>">
					<div class="section span_12_of_12 puff-info__container--archive">
						<span class="date">
							<?php
							if(get_field('datum_for_handelse')) :
								$date = get_field('datum_for_handelse');
								$dateformatstring = "d F Y";
								$unixtimestamp = strtotime($date);
								echo date_i18n($dateformatstring, $unixtimestamp);
							endif;
						?>

						</span>
						<?php
						$categories = get_the_category($postsToDisplay[$i]);
						$category_link = get_category_link( $categories[0]->term_id );
						?>
							<a class="btn__category" href="<?php echo $category_link; ?>"><?php echo $categories[0]->name; ?></a>
					</div>
	        <h2 class="category__header">
	          <a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
	        </h2>

	        <article class="">
	          <?php
	          $text = excerpt(35);
	          ?>
	          <p>
	            <?php echo $text; ?>
	          </p>
	          <a href="<?php echo the_permalink(); ?>"class="view-article">Läs mer ></a>
	        </article>
	      </section>
	    </section>

	  <?php endwhile; ?>
	<?php endif; ?>
	</section>
</main>
<?php


get_footer();
?>
