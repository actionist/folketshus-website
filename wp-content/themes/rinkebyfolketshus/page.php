<?php get_header(); ?>

	<main role="main" class="wrapper__main">
		<!-- section -->
		<section class="section span_12_of_12 page__wrapper">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<?php if ( has_post_thumbnail()) :
				$thumb_id = get_post_thumbnail_id($post->ID);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
				$thumb_url = $thumb_url_array[0];
			?>
				<div class="page__img" style="background-image:url(<?php echo $thumb_url; ?>);"></div>

				<?php endif; ?>
				<h1 class="page__header">
					<?php the_title(); ?>
				</h1>
			<!-- article -->
			<article class="page__content" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php the_content();?>

				<!-- <?php comments_template( '', true ); // Remove if you don't want comments ?> -->

				<br class="clear">

				<!-- <?php edit_post_link(); ?> -->

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		<?php if(is_page( 10 )) :?>
			<h2>Våra medlemmar</h2>
			<section class="section span_12_of_12 page__members-container">

			<?php
			$memberLoop = new WP_Query(array('post_type' => 'members', 'posts_per_page' => -1));

			if($memberLoop->have_posts() ) :
				while($memberLoop->have_posts() ) : $memberLoop->the_post();
				?>
				<div class="member__post" ><span class="member-logo__container"><?php echo get_the_post_thumbnail(); ?><h3 class="member-name"><?php echo the_title(); ?></h3></span></div>
				<?php
				endwhile;
			endif;

			$email;
			if(get_field('mail_member', 10) ) :
				$email = get_field('mail_member', 10);
			else :
				$email = 'rinkeby.folketshus@gmail.com';
			endif;
			$message = 'Jag vill bli medlem, kontakta mig!';
			?>
				<a href="mailto:<?php echo $email;?>?subject=<?php echo $message;?>" class="btn__member--page">Bli en av oss</a>
		<?php endif;?>

		</section>
		</section>
		<!-- /section -->
	</main>



<?php get_footer(); ?>
