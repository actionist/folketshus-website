<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<!--Site fonts -->
		<script src="https://use.typekit.net/gci4xbf.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<!--Font awesome-->
		<script src="https://use.fontawesome.com/107ef39adb.js"></script>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class('no-touch'); ?>>
		<!-- wrapper -->
		<div >

			<!-- header -->
			<header class="header clear section span_12_of_12 wrapper__main" ><!--role="banner"-->
				<a href="<?php echo home_url(); ?>" class="section span_2_of_12">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo" class="logo-img">
				</a>

				<nav class="nav section span_10_of_12" role="navigation">
						<?php html5blank_nav(); ?>
				</nav><!-- /nav -->

				<div class="menu__mobile" id="mobile-menu">
					<div class="bar__mobile"></div>
					<div class="bar__mobile"></div>
					<div class="bar__mobile"></div>
				</div><!-- /mobile menu -->
			</header>
			<!-- /header -->
