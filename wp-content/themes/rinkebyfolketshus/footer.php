			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<section class="section group footer__wrapper">
					<div class="logo__container--footer">
						<a href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-transparent.png" alt="Logo" class="logo--footer">
						</a>
					</div>
					<div class="span_12_of_12">
						<div class="footer__content span_7_of_12">
							<section class="col span_5_of_12">
								<nav class="footer__nav" role="navigation">
									<?php footer_nav(); ?>
								</nav><!-- /nav -->
							</section>
							<section class=" col span_7_of_12 footer__text">
								<p>
									<?php
									if(get_field('footer_text', 4)):
										echo get_field('footer_text', 4);
									endif;
									?>
								</p>
							</section>
						</div>

						<section class="span_5_of_12 map">
							<iframe
							width="600"
  						src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCJNRKI-gUl1MkQocKtlXEiuXj3ElLiEHY&q=Rinkeby+Folkets+Hus,@59.3888473,17.9248676&zoom=15"
							allowfullscreen>
							</iframe>
						</section>
					</div>
				</section>

				<!--  -->

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->
		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
