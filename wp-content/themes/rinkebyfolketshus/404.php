<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="wrapper__main">

			<!-- article -->
			<article id="post-404">

				<h1 class="page__header"><?php _e( 'Sidan verkar inte finnas', 'html5blank' ); ?></h1>
				<h2>
					<a class="page__link" href="<?php echo home_url(); ?>"><?php _e( 'Vill du gå tillbaka till start?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
