<?php
/* Template Name: Startsida */

get_header();
?>
<main role="main" style="clear:both;" class="">
	<div class="section group puff__wrapper wrapper__main"><!-- start__wrapper -->
<?php
if (have_posts()): while (have_posts()) : the_post();
	if(get_field('inlagg')) :
		$pinnedPost = get_field('inlagg', $post->ID);
	endif;

	if(get_field('fran_datum')) :
		$fromDate = strtotime(get_field('fran_datum'));
	endif;

	if(get_field('till_datum')) :
		$toDate = strtotime(get_field('till_datum'));
	endif;

	if(have_rows('bilder')) :
		$heroImgArray = [];
		while(have_rows('bilder')) : the_row();
			array_push($heroImgArray, get_sub_field('hero_img'));
		endwhile;
	endif;
	endwhile;
endif;

$postLoop = new WP_Query(array(
	'post_type' => 'post',
	'posts_per_page' => 2,
	'meta_key' => 'datum_for_handelse',
	'orderby' => 'meta_value',
	'order' => 'DESC'
));

$postsToDisplay = [];
$today = strtotime(date('Ymd'));

if(($pinnedPost) && ($today >= $fromDate) && ($today <= $toDate)){
	array_push($postsToDisplay, $pinnedPost->ID);
}

if($postLoop->have_posts() ) :
	while($postLoop->have_posts() ) : $postLoop->the_post();
	$id = get_the_ID();

	$postsLength = count($postsToDisplay);
	if(($id !== $pinnedPost->ID) && ($postsLength < 2)){
		array_push($postsToDisplay, $id);
	}
		endwhile;
	endif;

	$postsLength = count($postsToDisplay);
	$imgSwitch;
		for ($i=0; $i < $postsLength; $i++) {
			?>
			<section class="col span_6_of_12 puff__container">
				<div class="section span_12_of_12 puff-info__container">
					<span class="date">
						<?php
						if(get_field('datum_for_handelse')) :
							$date = get_post_meta($postsToDisplay[$i], 'datum_for_handelse', true);
							$dateformatstring = "d F Y";
							$unixtimestamp = strtotime(get_post_meta($postsToDisplay[$i], 'datum_for_handelse', true));
							echo date_i18n($dateformatstring, $unixtimestamp);
						endif;
					?>

					</span>
					<?php
					$categories = get_the_category($postsToDisplay[$i]);
					$category_link = get_category_link( $categories[0]->term_id );
					?>
						<a class="btn__category" href="<?php echo $category_link; ?>"><?php echo $categories[0]->name; ?></a>
				</div>

				<h2 class="puff__header">
					<a href="<?php echo get_permalink($postsToDisplay[$i]); ?>"><?php echo get_the_title($postsToDisplay[$i]); ?></a>
				</h2>
				<?php
				if(get_field('img_switch',  $postsToDisplay[$i])) :
					$imgSwitch = get_field('img_switch', $postsToDisplay[$i]);
					if($imgSwitch == true) {
						$thumb_id = get_post_thumbnail_id($postsToDisplay[$i]);
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
						$thumb_url = $thumb_url_array[0];
					?>
					<div class="puff-intro__container">
						<div class="puff__img" style="background-image:url(<?php echo $thumb_url; ?>)">
						</div>
				<?php
			}
				endif;

				if(($imgSwitch == true) && (get_field('bildtext', $postsToDisplay[$i]))) {
					$imgTxt = get_field('bildtext', $postsToDisplay[$i]);
					?>
						<p class="puff__intro"><?php echo $imgTxt; ?> </p>
						<a class="view-article" href="<?php echo get_permalink($postsToDisplay[$i]); ?>"><span>Läs mer ></span></a>
						</div>
					<?php
				} else {?>
					<div class="puff-intro__container">
						<p class="puff__intro"><?php echo the_excerpt($postsToDisplay[$i]); ?> </p>
					</div>
				<?php }
				?>
			</section>
			<?php
		}
		?>
		</div>

	<!-- Hero image  -->
		<div>
			<ul class="hero__container">
			<?php
			$id = 0;
		foreach ($heroImgArray as $image => $url) {
			$id++;
			if($id == 1) {
				$class = 'hero--active';
			} else {
				$class = 'hero--inactive';
			}
			?>
				<li class="section span_12_of_12 hero__img <?php echo $class; ?>" style="background-image: url(<?php echo $url; ?>)" id="<?php echo $id; ?>">
				</li>
		<?php } ?>
			</ul>
		</div> <!-- end hero__container-->

		<!-- 	-->
		<div class="section group  services__wrapper"> <!-- services -->
		<?php
		if (have_posts()): while (have_posts()) : the_post();?>
			<section class="section span_12_of_12 services__container ">
				<img class="icon--lecture" src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/lecture.svg" alt="<?php echo get_field('rubrik_lecture'); ?>"/>
				<section class="services__content">
					<h2 class="services__header">
						<?php
						if(get_field('rubrik_lecture')) :
							echo get_field('rubrik_lecture');
						endif;
						?>
					</h2>
					<p class="puff__intro">
					<?php if(get_field('pufftext_lecture')) :
						echo get_field('pufftext_lecture');
					endif; 	?>
					</p>
					<a href="<?php echo get_page_link(16); ?>" class="btn__services">
					<?php if(get_field('btn_lecture')) :
						echo get_field('btn_lecture');
					endif; ?>
					</a>
				</section>

				<img class="icon--venue" src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/room.svg" alt="<?php echo get_field('rubrik_venue'); ?>" />
				<section class="services__content">
					<h2 class="services__header">
						<?php
						if(get_field('rubrik_venue')) :
							echo get_field('rubrik_venue');
						endif;
						?>
					</h2>
					<p class="puff__intro">
						<?php
						if(get_field('pufftext_venue')) :
							echo get_field('pufftext_venue');
						endif;
						?>
					</p>
						<a href="<?php echo get_page_link(18); ?>" class="btn__services">
							<?php if(get_field('btn_venue')) :
								echo get_field('btn_venue');
							endif;
							?>
						</a>
				</section>
			</section>
			<?php

			endwhile;
		endif;
		?>


		</div>
		<?php
		$memberLoop = new WP_Query(array('post_type' => 'members', 'posts_per_page' => -1));
		$memberArray = [];
		?>
		<section class="section group members__wrapper">
			<h2 class="puff__header">Våra vänner <i class="fa fa-heart icon--heart" aria-hidden="true"></i></h2>
			<section class="section span_12_of_12 members__container">
				<?php

					if($memberLoop->have_posts() ) :
						while($memberLoop->have_posts() ) : $memberLoop->the_post();
						array_push($memberArray, $post->ID);
						endwhile;
					endif;
					$loopLength = count($memberArray);
					for ($i=0; $i < $loopLength; $i++) {
						if($i < 18) {
							$class = 'member--active';
						} else {
							$class = 'member--inactive';
						}
						?>
							<span class="icon--members <?php echo $class?>" id="<?php echo 'item-' . $i; ?>">
								<?php echo get_the_post_thumbnail($memberArray[$i]);?>
							</span>
					<?php }
				?>

			</section>
			<?php
			$email;
			if(get_field('mail_member', 10) ) :
				$email = get_field('mail_member', 10);
			else :
				$email = 'rinkeby.folketshus@gmail.com';
			endif;
			$message = 'Jag vill bli medlem, kontakta mig!';
			?>
				<a href="mailto:<?php echo $email;?>?subject=<?php echo $message;?>" class="btn__member">Bli en av oss</a>

		</section>
</main>

<?php get_footer(); ?>
