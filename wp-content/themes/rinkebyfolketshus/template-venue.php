<?php
/* Template Name: Lokaler */

get_header();
?>
<main role="main" class="wrapper__main">
	<section class="section span_12_of_12 category__wrapper">
		<?php
		if (have_posts()): while (have_posts()) : the_post(); ?>
			<h1 class="page__title"><?php the_title(); ?></h1>
			<p class="page__intro"><?php echo get_the_content(); ?></p>
	<?php
		endwhile;
		endif;
		?>

	  <?php
		$postLoop = new WP_Query(array(
			'post_type' => 'venues',
			'posts_per_page' => -1,
			'order' => 'DESC'
		));
		$posts = [];
		$counter;
		?>
		<section class="section span_12_of_12 puff__container--venue">
	<?php	if ($postLoop->have_posts()): while ($postLoop->have_posts()) : $postLoop->the_post();
		$counter++;
		array_push($posts, get_the_ID() );

	if(get_field('planritning', $postLoop->ID)):
		$venue_plan_img = get_field('planritning', $postLoop->ID);
	endif;
	endwhile; ?>
<?php endif;
	?>
	<?php
	$postsLength = count($posts);
	$allPosts = [];
	for($i = 0; $i < $postsLength; $i++) {
		$id = $posts[$i];

		if($i < $postsLength-1) {

			?>
			<div class="puff__venue span_1_of_12 venue--active" id="<?php echo $id; ?>">
				<a href="<?php echo get_the_permalink($posts[$i]); ?>" class="member__post--venue"  >
					<span class="venue-img__container"><img src="<?php echo get_field('planritning', $id); ?>" alt="<?php echo the_title($posts[$i]); ?>" />
						<h3 class="venue__name"><?php echo get_the_title($posts[$i]); ?></h3>
					</span>
				</a>
			</div>

		<?php
	} elseif(end($posts)) {
		$class = 'venue--inactive';
		?>
		<div class="puff__venue span_1_of_12 venue--inactive" id="<?php echo $id; ?>">
			<a href="<?php echo get_the_permalink($posts[$i]); ?>" class="member__post--venue" >
				<span class="venue-img__container"><img src="<?php echo get_field('planritning', $id); ?>" alt="<?php echo the_title($posts[$i]); ?>" />
					<h3 class="venue__name"><?php echo get_the_title($posts[$i]); ?></h3>
				</span>
			</a >
		</div>
		</section>
		<div class="section span_12_of_12 single-venue__container" id="single-venue" >
			<?php
			$postId = $id;
			include 'venue.php'; ?>
		</div>
	<?php }

	}
	?>
		<!-- <div class="puff__venue span_1_of_12" id="<?php echo get_the_ID(); ?>">
			<a href="<?php echo get_the_permalink($postLoop->ID); ?>" class="member__post--venue" >
				<span class="venue-img__container"><img src="<?php echo $venue_plan_img; ?>" alt="<?php echo the_title(); ?>" />
					<h3 class="member-name"><?php echo the_title(); ?></h3>
				</span>
			</a >
		</div> -->



	</section>
</main>
<?php


get_footer();
?>
