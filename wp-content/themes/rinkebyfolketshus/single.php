<?php get_header(); ?>

	<main role="main" class="wrapper__main">
	<!-- section -->
		<section class="section span_12_of_12 post__wrapper">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- post thumbnail -->
			<?php
				if(get_field('datum_for_handelse')):
					$date = get_field('datum_for_handelse');
					$dateformatstring = "d F Y";
					$unixtimestamp = strtotime(get_field('datum_for_handelse'));
			?>
			<p class="post__date">
				<?php echo date_i18n($dateformatstring, $unixtimestamp);?>
			</p>
			<?php if ( has_post_thumbnail()) :
				$thumb_id = get_post_thumbnail_id($post->ID);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
				$thumb_url = $thumb_url_array[0];
			?>
				<div class="post__img" style="background-image:url(<?php echo $thumb_url; ?>);"></div>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<?php endif;
				?>
			<h1 class="post__header">
				<?php the_title(); ?>
			</h1>
			<article class="post__content" id="post-<?php the_ID(); ?>">
				<?php the_content(); // Dynamic Content ?>
			</article>
			<!-- article -->
		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
	<!-- /section -->
	</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
