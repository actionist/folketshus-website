
<?php get_header(); ?>

	<main role="main" class="wrapper__main">
	<!-- section -->
		<section class="section group span_12_of_12 venue__wrapper">

		<?php
		if (have_posts()): while (have_posts()) : the_post(); ?>

			<section class="col span_7_of_12 venue__txt-col">

				<div class="span_12_of_12">
					<div class="  span_6_of_12 venue__info--container ">

							<?php
							if(get_field('antal_platser')):?>
								<span class="venue__info">
									<?php echo get_field('antal_platser');?>
								</span>
							<?php endif;
							?>

						<p class="venue__info-txt">
							Antal platser i lokalen.
						</p>
					</div>
					<div class="span_6_of_12  venue__info--container">
						<?php
						if(get_field('storlek_lokal')):?>
							<span class="venue__info">
								<?php echo get_field('antal_platser') . ' m2';?>
							</span>
						<?php endif;
						?>
						<p class="venue__info-txt">
							Storlek på lokalen.
						</p>
					</div>
				</div>
				<section class="span_12_of_12 venue__txt">
					<h1 class="puff__header"><?php the_title(); ?></h1>
					<?php
					if(get_field('info_txt_lokal') ) :?>
						<p>
							<?php echo get_field('info_txt_lokal'); ?>
						</p>
					<?php endif;
					?>
				</section>


					<?php
					if(get_field('mail_lokal')):
						$address = get_field('mail_lokal');
						$message = 'Jag är intresserad av att boka ' . get_the_title();
					endif;
					?>
					<a class="btn__book--venue" href="mailto:<?php echo $address; ?>?subject=<?php echo $message; ?>">Skicka förfrågan</a>

			</section>
			<section class="col span_5_of_12 venue__imgs-col">
				<?php
				if(get_field('planritning')):?>
					<div class="venue__img" style="background-image: url(<?php echo get_field('planritning'); ?>)">
					</div>
				<?php endif;
				if(have_rows('imgs_lokal')):
					while(have_rows('imgs_lokal')) : the_row();
					$imgUrl = get_sub_field('img_lokal');

					?>
					<div class="venue__img" style="background-image: url(<?php echo $imgUrl; ?>)">
					</div>
			<?php	endwhile;
			endif;
				?>
			</section>


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
	<!-- /section -->
	</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
