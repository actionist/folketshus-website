
<?php get_header(); ?>

	<main role="main" class="wrapper__main">
	<!-- section -->
		<section class="section group span_12_of_12 venue__wrapper">

		<?php

		if (have_posts()): while (have_posts()) : the_post(); ?>

			<section class="col span_7_of_12 venue__txt-col">

				<div class="span_12_of_12">
					<div class="span_6_of_12 venue__info--container ">

							<?php
							if(get_field('speaker')):?>
								<span class="venue__info">
									<?php echo get_field('speaker');?>
								</span>
							<?php endif;
							?>
							<?php
							if(get_field('title_speaker')):?>
						<p class="venue__info-txt">
							<?php echo get_field('title_speaker'); ?>
						</p>
					<?php endif;
					?>
					</div>

				</div>
				<section class="span_12_of_12 venue__txt">
					<h1 class="puff__header"><?php the_title(); ?></h1>
					<p><?php echo the_content(); ?></p>
				</section>

					<?php
					if(get_field('mail_lecture')):
						$address = get_field('mail_lecture');
						$message = 'Jag vill boka in mig på: ' . get_the_title();
					endif;
					?>
					<a class="btn__book--venue" href="mailto:<?php echo $address; ?>?subject=<?php echo $message; ?>">Boka in mig på detta</a>
				<!-- </section> -->
			</section>
			<section class="col span_5_of_12 venue__imgs-col">
				<?php
				if(have_rows('imgs_lecture')):
					while(have_rows('imgs_lecture')) : the_row();
					$imgUrl = get_sub_field('img_lecture');

					?>
					<div class="venue__img" style="background-image: url(<?php echo $imgUrl; ?>)">
					</div>
			<?php	endwhile;
			endif;
				?>
			</section>


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
	<!-- /section -->
	</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
