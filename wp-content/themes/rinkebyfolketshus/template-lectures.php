<?php
/* Template Name: Föreläsningar */

get_header();
?>
<main role="main" class="wrapper__main">
	<section class="section span_12_of_12 category__wrapper">
		<?php
		if (have_posts()): while (have_posts()) : the_post(); ?>
			<h1 class="page__title"><?php the_title(); ?></h1>
			<p class="page__intro"><?php echo get_the_content(); ?></p>
	<?php
 endwhile;
 	endif;
		?>

	  <?php
		$postLoop = new WP_Query(array(
			'post_type' => 'lectures',
			'posts_per_page' => -1,
		));
		if ($postLoop->have_posts()): while ($postLoop->have_posts()) : $postLoop->the_post(); ?>
	    <?php

			$count++;
			$even_odd_class = ( ($count % 2) == 0 ) ? "even" : "odd";

			if(have_rows('imgs_lecture')):
				$imgs = [];
				while( have_rows('imgs_lecture') ) : the_row();
				$img = get_sub_field('img_lecture');
				array_push($imgs, $img);
			endwhile;
			endif;
	    ?>

	    <section class="section span_12_of_12 category__post <?php echo $even_odd_class; ?>">
	      <?php if ( !empty($imgs) ) : ?>
	        <div class="category__img span_4_of_12" style="background-image:url(<?php echo $imgs[0]; ?>);">
	        </div>
	      <?php
				$section_spans = 'span_8_of_12';
				else :
				$section_spans = 'span_12_of_12';

			endif;
	      ?>
	      <section class="category__content <?php echo $section_spans; ?>">
	        <h2 class="category__header">
	          <a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>

	        </h2>
	        <article class="">
	          <?php
	          $text = excerpt(40);
	          ?>
	          <p>
	            <?php echo $text; ?>
	          </p>
	          <a href="<?php echo the_permalink(); ?>"class="view-article">Läs mer ></a>
	        </article>
	      </section>
	    </section>

	  <?php endwhile; ?>
	<?php endif; ?>
	</section>
</main>
<?php


get_footer();
?>
