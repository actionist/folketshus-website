<section class="section group span_12_of_12 venue__wrapper" id="<?php echo $postId; ?>">
  <section class="col span_7_of_12 venue__txt-col">
    <div class="span_12_of_12">
      <div class="span_6_of_12 venue__info--container ">

          <?php
          if(get_field('antal_platser', $postId)):?>
            <span class="venue__info">
              <?php echo get_field('antal_platser', $postId);?>
            </span>
            <span class="icon--seats">
            </span>
          <?php endif;
          ?>

        <p class="venue__info-txt">
          Antal platser i lokalen.
        </p>
      </div>
      <div class="span_6_of_12 venue__info--container">
        <?php
        if(get_field('storlek_lokal', $postId)):?>
          <span class="venue__info">
            <?php echo get_field('antal_platser', $postId) . ' m2';?>
          </span>
        <?php endif;
        ?>
        <p class="venue__info-txt">
          Storlek på lokalen.
        </p>
      </div>
    </div>
    <section class="span_12_of_12 venue__txt">
      <h1 class="puff__header"><?php echo get_the_title($postId); ?></h1>
      <?php
      if(get_field('info_txt_lokal', $postId) ) :?>
        <p>
          <?php echo get_field('info_txt_lokal', $postId); ?>
        </p>
      <?php endif;
      ?>
    </section>


      <?php
      if(get_field('mail_lokal', $postId)):
        $address = get_field('mail_lokal', $postId);
        $message = 'Jag är intresserad av att boka ' . get_the_title($postId);
      endif;
      ?>
      <a class="btn__book--venue" href="mailto:<?php echo $address; ?>?subject=<?php echo $message; ?>">Skicka förfrågan</a>

  </section>
  <section class="col span_5_of_12 venue__imgs-col">
    <?php
    if(get_field('planritning', $postId)):?>
      <div class="venue__img" style="background-image: url(<?php echo get_field('planritning', $postId); ?>)">
      </div>
    <?php endif;
    if(have_rows('imgs_lokal', $postId)):
      while(have_rows('imgs_lokal', $postId)) : the_row();
      $imgUrl = get_sub_field('img_lokal', $postId);
      ?>
      <div class="venue__img" style="background-image: url(<?php echo $imgUrl; ?>)">
      </div>
  <?php	endwhile;
  endif;
    ?>
  </section>
</section>
